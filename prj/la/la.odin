package la

import "core:math"
import "core:fmt" // @Temp for debugging

// Note: For all matrix-vector multiplications, the matrix MUST go first
// An example:

// m: mat4
// v: v4f
// result := m * v
// return result

PI :: 3.141592654

v3f :: [3]f32
v4f :: [4]f32

mat4 :: matrix[4, 4] f32
mat3 :: matrix[3, 3] f32

//--------------------------------------------------------------------------------
// Matrix part
//--------------------------------------------------------------------------------

// Identity
//--------------------------------------------------------------------------------
MakeIdentity_mat4 :: proc() -> mat4
{
	Result: mat4 = 1
	return Result
}

MakeIdentity :: proc {MakeIdentity_mat4}

// Translation
//--------------------------------------------------------------------------------
MakeTranslation_mat4_v4 :: proc(m: mat4, v: v4f) -> mat4
{
	result: mat4 = m
	result[3][0] = v.x
	result[3][1] = v.y
	result[3][2] = v.z
	return result
}

MakeTranslation_mat4_v3 :: proc(m: mat4, v: v3f) -> mat4
{
	// This assumes we have an identity matrix!
	result: mat4 = m
	result[3][0] = v.x
	result[3][1] = v.y
	result[3][2] = v.z
	return result
}

MakeTranslation :: proc{MakeTranslation_mat4_v4, MakeTranslation_mat4_v3}


Translate_mat4_v3 :: proc(m: mat4, v: v3f) -> v3f
{
	result := mat3(m) * v
	return result
}

Translate_mat4_v4 :: proc(m: mat4, v: v4f) -> v4f
{
	result := m * v
	return result
}

Translate :: proc{Translate_mat4_v3, Translate_mat4_v4}

// Scale
//--------------------------------------------------------------------------------

MakeScale_mat4_v4 :: proc(m: mat4, v: v4f) -> mat4
{
	result := m
	result[0][0] = v.x
	result[1][1] = v.y
	result[2][2] = v.z
	return result

}

MakeScale_mat4_v3 :: proc(m: mat4, v: v3f) -> mat4
{
	result := m
	result[0][0] = v.x
	result[1][1] = v.y
	result[2][2] = v.z
	return result

}

MakeScale :: proc {MakeScale_mat4_v3, MakeScale_mat4_v4}

Scale_mat4_v4 :: proc(m: mat4, v: v4f) -> v4f
{
	result := m * v
	return result
}

Scale_mat4_v3 :: proc(m: mat4, v: v3f) -> v3f
{
	result := mat3(m) * v
	return result
}

Radians :: proc(degrees: f32) -> f32
{
	return degrees * (PI / 180.0)
}

// Rotation
//--------------------------------------------------------------------------------
MakeRotationX_mat4 :: proc(m: mat4, radians: f32) -> mat4
{
	using math
	result: mat4
	result = m

	result[0][0] = 1
	result[1][1] = cos(radians)
	result[1][2] = sin(radians)
	result[2][1] = -sin(radians)
	result[2][2] = cos(radians)
	result[3][3] = 1

	return result
}

MakeRotationZ_mat4 :: proc(m: mat4, radians: f32) -> mat4
{
	using math
	result: mat4
	result = m
	
	result[0][0] = cos(radians)
	result[0][1] = sin(radians)
	result[1][0] = -sin(radians)
	result[1][1] = cos(radians)
	result[2][2] = 1
	result[3][3] = 1
	
	return result
}

MakeRotationY_mat4 :: proc(m: mat4, radians: f32) -> mat4
{
	using math
	result: mat4 = m

	result[0][0] = cos(radians)
	result[0][2] = -sin(radians)
	result[1][1] = 1
	result[2][0] = sin(radians)
	result[2][2] = cos(radians)
	result[3][3] = 1
	
	return result
}

// Taken from the odin linalg library
MakeRotation_mat4 :: proc(radians: f32, v: v3f) -> mat4
{
	c := math.cos(radians)
	s := math.sin(radians)

	a := Normalise(v)
	t := a * (1-c)

	rot := MakeIdentity()

	rot[0][0] = c + t[0]*a[0]
	rot[0][1] = 0 + t[0]*a[1] + s*a[2]
	rot[0][2] = 0 + t[0]*a[2] - s*a[1]
	rot[0][3] = 0

	rot[1][0] = 0 + t[1]*a[0] - s*a[2]
	rot[1][1] = c + t[1]*a[1]
	rot[1][2] = 0 + t[1]*a[2] + s*a[0]
	rot[1][3] = 0

	rot[2][0] = 0 + t[2]*a[0] + s*a[1]
	rot[2][1] = 0 + t[2]*a[1] - s*a[0]
	rot[2][2] = c + t[2]*a[2]
	rot[2][3] = 0

	return rot
}


Rotate_mat4_v4f :: proc(m: mat4, v: v4f) -> v4f
{
	result := m * v
	return result
}

MakeRotationX :: proc{MakeRotationX_mat4}
MakeRotationY :: proc{MakeRotationY_mat4}
MakeRotationZ :: proc{MakeRotationZ_mat4}
MakeRotation :: proc{MakeRotation_mat4}

// Projection
//--------------------------------------------------------------------------------
// Special thanks to GLM team for posting the source!
// It really helped with the projection matrix
MakePerspective_mat4_lhzo :: proc(fovRadians, aspect, near, far: f32) -> mat4
{
	using math
	result: mat4 = MakeIdentity()
	fov:= fovRadians
	tanHalfFovy := tan(fov / 2.0);
	result[0][0] = 1 / (aspect * tanHalfFovy);
	result[1][1] = 1.0 / (tanHalfFovy);
	result[2][2] = far / (far - near);
	
	result[2][3] = 1;
	result[3][2] = -(far * near) / (far - near);

	return result
}

MakePerspective_mat4_lhno :: proc(fovRadians, aspect, near, far: f32) -> mat4
{
	using math
	result: mat4 = MakeIdentity()
	fov:= fovRadians
	tanHalfFovy := tan(fov / 2.0);
	result[0][0] = 1 / (aspect * tanHalfFovy);
	result[1][1] = 1.0 / (tanHalfFovy);
	result[2][2] = (far + near) / (far - near);
	result[2][3] = 1;
	result[3][2] = (2 * far * near) / (far - near);
	
	return result
}

MakePerspective_mat4_rhzo :: proc(fovRadians, aspect, near, far: f32) -> mat4
{
	using math
	result: mat4 = MakeIdentity()
	fov:= fovRadians
	tanHalfFovy := tan(fov / 2.0);
	result[0][0] = 1 / (aspect * tanHalfFovy);
	result[1][1] = 1.0 / (tanHalfFovy);
	result[2][2] = far / (far - near);
	result[2][3] = -1;
	result[3][2] = -(far * near) / (far - near);

	return result
}

// This is the one (rhno)!! (for learnopengl, at least)
MakePerspective_mat4_rhno :: proc(fovRadians, aspect, near, far: f32) -> mat4
{
	using math

	// Note to self: Result must be initialised to zero!!!
	// Wasted a bunch of time tracking this down, but oh well. At least we learned something :/
	result: mat4
	fov:= fovRadians
	tanHalfFovy := tan(fov / 2.0);
	result[0][0] = 1.0 / (aspect * tanHalfFovy);
	result[1][1] = 1.0 / (tanHalfFovy);
	result[2][2] = -(far + near) / (far - near);
	
	result[2][3] = -1;
	result[3][2] = -(2 * far * near) / (far - near);
	return result
}

// This one! (rhno)
MakePerspective :: proc{MakePerspective_mat4_rhno}

// Look at
//--------------------------------------------------------------------------------
LookAt :: proc(position, target, up: v4f) -> mat4
{
	direction: v4f = Normalise(position - target)
	right: v4f = Normalise(CrossProduct(up, direction))
	upInternal: v4f = CrossProduct(direction, right)

	lookLhs: mat4 = 
	{
		right.x, right.y, right.z, 0,
		upInternal.x, upInternal.y, upInternal.z, 0,
		direction.x, direction.y, direction.z, 0,
		0, 0, 0, 1,
	}

	lookRhs: mat4 = 
	{
		1, 0, 0, -position.x,
		0, 1, 0, -position.y,
		0, 0, 1, -position.z,
		0, 0, 0, 1,
	}

	result := lookLhs * lookRhs
	return result
}

//--------------------------------------------------------------------------------
// Vector part
//--------------------------------------------------------------------------------

// Square
//--------------------------------------------------------------------------------
Square_v4 :: proc(v: v4f) -> v4f
{
	result: v4f
	w := v.w
	imm := v * v
	imm.w = w
	result  = imm
	return result
}

Square :: proc{Square_v4}

// Len
//--------------------------------------------------------------------------------
Len_v4 :: proc(v: v4f) -> f32
{
	result: f32
	imm1: v4f = v * v
	result = math.sqrt(imm1.x + imm1.y + imm1.z)
	return result
}

Len_v3 :: proc(v: v3f) -> f32
{
	result: f32
	imm1 := v * v
	result = math.sqrt(imm1.x + imm1.y + imm1.z)
	return result
}


Len :: proc{Len_v3, Len_v4}

// Dot product
//--------------------------------------------------------------------------------
DotProduct_v4 :: proc(a, b: v4f) -> f32
{
	result: f32
	imm1 := a.xyz * b.xyz
	result = imm1.x + imm1.y + imm1.z
	return result
}

DotProduct_v3 :: proc(a, b: v3f) -> f32
{
	result: f32
	imm1 := a * b
	result = imm1.x + imm1.y + imm1.z
	return result
}

DotProduct :: proc{DotProduct_v3, DotProduct_v4}

// Cross product
//--------------------------------------------------------------------------------
CrossProduct_v4 :: proc(a, b: v4f) -> v4f
{
	result: v4f
	result.x = (a.y * b.z) - (a.z * b.y)
	result.y = (a.z * b.x) - (a.x * b.z)
	result.z = (a.x * b.y) - (a.y * b.x)
	return result
}

CrossProduct_v3 :: proc(a, b: v3f) -> v3f
{
	result: v3f
	result.x = (a.y * b.z) - (a.z * b.y)
	result.y = (a.z * b.x) - (a.x * b.z)
	result.z = (a.x * b.y) - (a.y * b.x)
	return result
}

CrossProduct :: proc{CrossProduct_v3, CrossProduct_v4}

// Normalise
//--------------------------------------------------------------------------------
Normalise_v4 :: proc(v: v4f) -> v4f
{
	w:= v.w
	result := v / Len(v)
	result.w = w
	return result
}

Normalise_v3 :: proc(v: v3f) -> v3f
{
	result := v / Len(v)
	return result
}

Normalise :: proc{Normalise_v3, Normalise_v4}

// Direction (Camera controls)
//--------------------------------------------------------------------------------

// Do we want to convert to radians here, or externally?
Direction :: proc(pitch, yaw: f32) -> v4f
{
	result: v4f
	using math
	yaw := Radians(yaw)
	pitch := Radians(pitch)

	result.x = cos(yaw) * cos(pitch)
	result.y = sin(pitch)
	result.z = sin(yaw) * cos(pitch)
	return result
}

