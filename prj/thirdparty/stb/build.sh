# Commands to build a static library of stb_image.h

# Once built, put the resulting file 'stb_image.a' in the 'lib' folder of the project

base="stb_image"
src="$base.h"
obj="$base.o"
lib="$base.a"

# Your favourite compiler here
cc="gcc"

$cc -x c -c "$src" -DSTB_IMAGE_IMPLEMENTATION -fPIC -Os 

ar rcs "$lib" "$obj"
