package sl

import "core:fmt"
import "core:os"
import "core:strings"

import gl "prj:thirdparty/gl"
import glfw "vendor:glfw"

// import "prj:thirdparty/gl"
// import "prj:thirdparty/glfw"

// Configure this to control if we crash if a uniform is not found
// Set to true by default
VerifyUniformAccessEnabled :: false 




@private
Die:= fmt.assertf

program_type :: u32
shader_type :: i32
handle_type :: i32

uniform_info :: map[string]i32


vtable :: struct
{
	Uniform1f: type_of(Uniform1f_Impl), 
	Uniform2f: type_of(Uniform2f_Impl), 
	Uniform3f: type_of(Uniform3f_Impl), 

	// Uniform1fv: type_of(Uniform1f_Impl), 
	// Uniform2fv: type_of(Uniform2f_Impl), 
	Uniform3fv: type_of(Uniform3fv_Impl), 

	Uniform1i: type_of(Uniform1i_Impl), 
	Uniform2i: type_of(Uniform2i_Impl), 
	Uniform3i: type_of(Uniform3i_Impl), 

	UniformMatrix4fv: type_of(UniformMatrix4fv_Impl),
	UniformMatrix4f: type_of(UniformMatrix4f_Impl),

	Use: type_of(Use_Impl),
}

@private
GlobalFunctionVtable: vtable = VtableInit()

shader_info :: struct
{
	program: program_type,
	vars: uniform_info,

	using table: ^vtable,
}

Init :: proc() -> (info: ^shader_info)
{
	info = new(shader_info)
	info.table = &GlobalFunctionVtable
	return
}

VtableInit :: proc() -> (table: vtable)
{
	using table
// Uniforms
//--------------------------------------------------------------------------------
	Uniform1f = Uniform1f_Impl
	Uniform2f = Uniform2f_Impl
	Uniform3f = Uniform3f_Impl

	Uniform3fv = Uniform3fv_Impl

	Uniform1i = Uniform1i_Impl
	Uniform2i = Uniform2i_Impl
	Uniform3i = Uniform3i_Impl

	

	UniformMatrix4f = UniformMatrix4f_Impl
	
	UniformMatrix4fv = UniformMatrix4fv_Impl

// Misc
//--------------------------------------------------------------------------------
	Use = Use_Impl

	return
}

UniformVerify :: proc(vars: uniform_info, identifier: string, loc := #caller_location) -> (handle: handle_type)
{
	ok: bool
	handle, ok = vars[identifier]
	if VerifyUniformAccessEnabled
	{
		Die(ok == true,"{}: Tried to search for glsl variable '{}', but got back an invalid id of '{}'\n", loc, identifier, handle)
	}


	return
}


// Int(explicit)
//--------------------------------------------------------------------------------
Uniform1i_Impl :: proc(Info: ^shader_info, identifier: string, x: i32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform1i(loc, x)
}

Uniform2i_Impl :: proc(Info: ^shader_info, identifier: string, x, y: i32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform2i(loc, x, y)
}

Uniform3i_Impl :: proc(Info: ^shader_info, identifier: string, x, y, z: i32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform3i(loc, x, y, z)
}

// Float(explicit)
//--------------------------------------------------------------------------------
Uniform1f_Impl :: proc(Info: ^shader_info, identifier: string, x: f32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform1f(loc, x)
}

Uniform2f_Impl :: proc(Info: ^shader_info, identifier: string, x, y: f32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform2f(loc, x, y)
}

Uniform3f_Impl :: proc(Info: ^shader_info, identifier: string, x, y, z: f32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform3f(loc, x, y, z)
}


// Float(variable)
//--------------------------------------------------------------------------------
Uniform3fv_Impl :: proc(Info: ^shader_info, identifier: string, count: i32, v: [^]f32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.Uniform3fv(loc, count, v)
}

// Matrix (explicit)
//--------------------------------------------------------------------------------
// Note: Not a standard OpenGL matrix function
UniformMatrix4f_Impl :: proc(Info: ^shader_info, identifier: string, data: [^]f32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.UniformMatrix4fv(loc, 1, false, data)
}

// Matrix (variable)
//--------------------------------------------------------------------------------
UniformMatrix4fv_Impl :: proc(Info: ^shader_info, identifier: string, count: i32, transpose: bool, data: [^]f32)
{
	loc := UniformVerify(Info.vars, identifier)
	gl.UniformMatrix4fv(loc, count, transpose, data)
}


// Misc
//--------------------------------------------------------------------------------
Use_Impl :: proc(Info: ^shader_info)
{
	gl.UseProgram(Info.program)
}


ShaderLoadFromBinary :: proc(vs, fs: string) -> (vars: uniform_info, program: program_type)
{
	ok: bool
	program, ok = gl.load_shaders_source(vs, fs)

	if !ok
	{
		fmt.eprintln("Failed to compile shaders!")
		os.exit(1)
	}

	vars = UniformInfoGetFromProgram(program)
	return
}

UniformInfoGetFromProgram :: proc(program: program_type) -> (vars: uniform_info)
{
	numUniforms: i32
	
	gl.UseProgram(program)
	gl.GetProgramiv(program, gl.ACTIVE_UNIFORMS, &numUniforms)
	
	bufsize :: 256
	buffer: [bufsize]u8


	for i in 0..<numUniforms
	{
		length, size: i32
		type: u32
		gl.GetActiveUniform(program, cast(u32)i, bufsize, &length, &size, &type, &buffer[0])
		s: string
		s = strings.clone(string(buffer[:length]))

		id := gl.GetUniformLocation(program, strings.unsafe_string_to_cstring(s))

		assert(id == i)

		vars[s] = i
	}
	return
}

LoadFromFile :: proc(vs, fs: string)
{
	unimplemented("Loading shaders from a file is not implemented yet!")
}

