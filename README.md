# OpenGL 3D Engine Demo

This is a demo of a basic 3D engine done in OpenGL.

At the time of writing, this has been among my first serious forays into 3D graphics programmming.

I decided to jump right in to [learnopengl](www.learnopengl.com) and have a go at it.

I also made it a bit more interesting by programming in a language I have never used before, [Odin](www.odin-lang.org).

The demo is a slight modification of `Chapter 24 (Blending)` from `learnopengl`. While finishing up the code, I was hit by a wave of inspiration, and decided to retexture everything to use art from the game [Knossu](https://jonathanwhiting.com/games/knossu/).

![thumbnail](./thumbnail.png)

## Core Features
* 3D camera
* Mouse look
* WASD controls
* Texturing
* Post-processing capabilities
* Offscreen render targets
* Basic transparency
* Basic linear algebra library included

## Controls
- `WASD`        - Move around
- `Mouse`       - Look around
- `Mousewheel`  - Zoom in/out (decrease/increase fov)
- `f`           - Toggle fullscreen
- `c`           - Toggle `knossu` mode
- `v`           - Toggle wireframe mode
- `Space`       - Fly up
- `Left-Shift`  - Fly down
- `q`           - Quit


## Building
Assuming you have all of the dependencies sorted out:
```console
# Linux

./build.sh
./demo
```

**Otherwise, dependencies are described below.**

### Odin
You will need the [Odin Compiler](https://odin-lang.org/docs/install/).

At the time of writing, the latest version is `Odin: dev-2022-03:fd415f0b`

### stb_image
Odin has bindings for `stb_image`. The library itself is included as the static library `stb_image.a`.

If you need to recompile it for your target machine, please see [build.sh](./prj/thirdparty/stb/build.sh). Instructions are in the build file.

### OpenGL 
Odin has an `OpenGL` bindings library, as well as `GLAD` to load functions during runtime. Odin does provide these as part of the `vendor` library collection, but they are included with the project explicitly for portability. However, no static nor dynamic library is included, you are responsible for setting this up on your machine.

Note: This program requires hardware support for an OpenGL **3.3** context.


### GLFW3
Odin has a `glfw` library. The project uses the `vendor:glfw` package provided as part of the compiler.

## Building on

### Linux
Once you have installed the compiler and set it in your `PATH`, run the following:
```console
# Build
odin build demo.odin -collection:prj=prj -out:demo

# Alternatively
./build.sh

# Run!
./demo
```

### Windows / MacOS
I have not built this project on either of these operating systems so I cannot give detailed instructions on how to compile for them.

However, Odin is supported on both Windows and Mac. Following similar build steps as described above may yield a working binary!

## Final notes
See `notes.txt` for some of my thoughts on the Odin language.

## References:
Textures sourced from **learnopengl** and **Knossu**

[learnopengl.com](https://www.learnopengl.com)

[Knossu](https://jonathanwhiting.com/games/knossu/)

[Odin](https://www.odin-lang.org)

[stb_image](https://github.com/nothings/stb)

[javidx9 - 3D Graphics Engine in C++](https://www.youtube.com/watch?v=ih20l3pJoeU)
