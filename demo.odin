package learn

import "core:fmt"
import "core:os"
import "core:mem"
import "core:math"
import "core:sort"
import "core:slice"
import "core:intrinsics"
import "core:runtime"

// Thirdparty
import glfw "vendor:glfw"
import gl   "prj:thirdparty/gl"
import stbi "prj:thirdparty/stb"

// Local
import sl "prj:shader_loader"
import la "prj:la"

/**********************************
* Constants
**********************************/
log :: fmt.printf
v4f :: la.v4f
v3f :: la.v3f
identity :: la.mat4(1)

/**********************************
* Types
**********************************/
glfw_context :: struct
{
	Window: glfw.WindowHandle,
	Monitor: glfw.MonitorHandle,
	Vidmode: ^glfw.VidMode,
}

globals :: struct
{
	timeToQuit: bool,
	filterEnabled: bool,
	Window: window,
}

window :: struct
{
	fh, fw: i32,
	ww, wh: i32,
	x, y: i32,
	wCurrent, hCurrent: i32,
	aspect: f32,
}

gl_texture_2d :: struct
{
	id: u32,
	w, h: i32,
	channels: i32,
	size: u32,
}


camera :: struct
{
	pos, front, up: v4f,
	fov: f32,
	yaw, pitch: f32,
}

window_mesh :: struct
{
	pos: v3f,
	distance: f32,
}

offscreen_buffer :: struct
{	
	fb: u32,
	rbo: u32,
	texId: u32,
	width, height: i32,
}

screen_quad :: struct
{
	vbo, vao: u32,
	using Shader: ^sl.shader_info,
}


/**********************************
* Globals
**********************************/
Globals: globals = {timeToQuit = false}
GlfwContext: glfw_context
Window: window
Camera: camera = CameraInit()
pFilterShader: ^sl.shader_info


QuadStride :: 4 * size_of(f32)
quadVertices: [24]f32 = 
{ 
	// positions 	// texCoords
	-1.0,  1.0,  	0.0, 1.0,
	-1.0, -1.0,  	0.0, 0.0,
	 1.0, -1.0,  	1.0, 0.0,

	-1.0,  1.0,  	0.0, 1.0,
	 1.0, -1.0,  	1.0, 0.0,
	 1.0,  1.0,  	1.0, 1.0,
}


main :: proc()
{
	// Initialize glfw
	glfw.Init()

	glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR, 3)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR, 3)
	glfw.WindowHint(glfw.SAMPLES, 4)
	{
		using GlfwContext
		Monitor = glfw.GetPrimaryMonitor()
		Vidmode = glfw.GetVideoMode(Monitor)
		Window = glfw.CreateWindow(Vidmode.width, Vidmode.height, "Scene", Monitor, nil)
		glfw.MakeContextCurrent(Window)
	}
	gl.load_up_to(3, 3, glfw.gl_set_proc_address)


	// Set window dimensions
	Globals.Window.fw = GlfwContext.Vidmode.width
	Globals.Window.fh = GlfwContext.Vidmode.height
	Globals.Window.ww = GlfwContext.Vidmode.width / 4
	Globals.Window.wh = GlfwContext.Vidmode.height / 4

	// Callbacks
	glfw.SetKeyCallback(GlfwContext.Window, auto_cast CbKeys)
	glfw.SetFramebufferSizeCallback(GlfwContext.Window, auto_cast CbResize)
	glfw.SetCursorPosCallback(GlfwContext.Window, auto_cast CbMouse)
	glfw.SetScrollCallback(GlfwContext.Window, auto_cast CbScroll)
	glfw.SetWindowCloseCallback(GlfwContext.Window, auto_cast CbClose)

	glfw.SetInputMode(GlfwContext.Window, glfw.CURSOR, glfw.CURSOR_DISABLED)

	// Initalize OpenGL state
	gl.Enable(gl.TEXTURE_2D)
	gl.Enable(gl.DEPTH_TEST)
	
	gl.Enable(gl.BLEND)
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	
	// Load textures

	CubeTextureInside := Texture2DLoadFromFile("prj/resources/knossu1.png", true, gl.REPEAT, gl.NEAREST)
	CubeTextureOutside := Texture2DLoadFromFile("prj/resources/knossu2.png", true, gl.REPEAT, gl.NEAREST)
	
	// FloorTexture := Texture2DLoadFromFile("prj/resources/metal.png", true, gl.REPEAT, gl.NEAREST)
	FloorTexture := Texture2DLoadFromFile("prj/resources/knossu_floor1.png", true, gl.REPEAT, gl.NEAREST)
	
	WindowTexture := Texture2DLoadFromFile("prj/resources/window.png", false, gl.CLAMP_TO_EDGE, gl.NEAREST)
	// WindowTexture := Texture2DLoadFromFile("prj/resources/knossu_window1.png", false, gl.CLAMP_TO_EDGE, gl.NEAREST)

	// WoodTexture := Texture2DLoadFromFile("prj/resources/wood.png", false, gl.CLAMP_TO_EDGE, gl.NEAREST)
	
	SkyTexture := Texture2DLoadFromFile("prj/resources/sky.png", false, gl.CLAMP_TO_EDGE, gl.NEAREST)


	// Create/Load shaders
	vertexCube ::
	`
	#version 330 core
	layout (location = 0) in vec3 aPos;
	layout (location = 1) in vec2 aTexCoords;

	out vec2 v_f_TexCoords;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	void main()
	{
		gl_Position = projection * view * model * vec4(aPos, 1);
		v_f_TexCoords = aTexCoords;
	}
	`

	fragCube ::
	`
	#version 330 core
	in vec2 v_f_TexCoords;
	out vec4 FragColor;

	uniform sampler2D texture1;
	uniform sampler2D texture2;

	float near = 0.1;
	float far = 100.0;

	float LinearizeDepth(float near, float far, float depth)
	{
		float z = depth * 2.0 - 1.0;
		return (2 * near * far) / (far + near - z * (far - near));
	}

	void main()
	{
		vec4 c;
		if(gl_FrontFacing)
		{
			c = texture(texture2, v_f_TexCoords);
		}
		else
		{
			c = texture(texture1, v_f_TexCoords);
		}
		FragColor = c;
	}
	`

	fragWindow ::
	`
	#version 330 core
	in vec2 v_f_TexCoords;
	out vec4 FragColor;

	uniform sampler2D texture1;
	uniform float red;
	
	void main()
	{
		vec4 c = texture(texture1, v_f_TexCoords);
		c.r = red;
		FragColor = c;
	}
	`

	vertexFilter ::
	`
	#version 330 core
	layout (location = 0) in vec2 aPos;

	void main()
	{
		gl_Position = vec4(aPos, 0, 1);
	}
	`

	fragFilter ::
	`
	#version 330 core
	out vec4 FragColor;

	uniform float alpha;
	void main()
	{
		vec2 rg = vec2(0.9, 0.8);
		FragColor = vec4(vec2(rg), 0.1, alpha);
	}
	`

	vertexSky ::
	`
	#version 330 core
	layout (location = 0) in vec2 aPos;
	layout (location = 1) in vec2 aTexCoords;

	uniform mat4 view;
	uniform mat4 projection;

	out vec2 TexCoords;
	void main()
	{
		gl_Position = vec4(aPos, 0.9999, 1); // No idea why this works, but it does
		TexCoords = aTexCoords;
	}
	`

	fragSky ::
	`
	#version 330 core
	out vec4 FragColor;

	in vec2 TexCoords;

	uniform float alpha;
	uniform sampler2D texture1;
	void main()
	{
		vec3 c = vec3(texture(texture1, TexCoords));
		FragColor = vec4(c, 1);
	}
	`

	CubeShader:= sl.Init()
	CubeShader.vars, CubeShader.program = sl.ShaderLoadFromBinary(vertexCube, fragCube)

	WindowShader:= sl.Init()
	WindowShader.vars, WindowShader.program = sl.ShaderLoadFromBinary(vertexCube, fragWindow)

	FilterShader:= sl.Init()
	FilterShader.vars, FilterShader.program = sl.ShaderLoadFromBinary(vertexFilter, fragFilter)
	pFilterShader = FilterShader

	SkyShader:= sl.Init()
	SkyShader.vars, SkyShader.program = sl.ShaderLoadFromBinary(vertexSky, fragSky)


	// Points
	// Used directly from face_culling_exercise1.cpp (learnopengl)
	CubeStride :: 5 * size_of(f32)
	cubeVertices: []f32  = 
	{
		// back face
		-0.5, -0.5, -0.5,  0.0, 0.0, // bottom-left
		 0.5, -0.5, -0.5,  1.0, 0.0, // bottom-right    
		 0.5,  0.5, -0.5,  1.0, 1.0, // top-right              
		 0.5,  0.5, -0.5,  1.0, 1.0, // top-right
		-0.5,  0.5, -0.5,  0.0, 1.0, // top-left
		-0.5, -0.5, -0.5,  0.0, 0.0, // bottom-left                
		// front face
		-0.5, -0.5,  0.5,  0.0, 0.0, // bottom-left
		 0.5,  0.5,  0.5,  1.0, 1.0, // top-right
		 0.5, -0.5,  0.5,  1.0, 0.0, // bottom-right        
		 0.5,  0.5,  0.5,  1.0, 1.0, // top-right
		-0.5, -0.5,  0.5,  0.0, 0.0, // bottom-left
		-0.5,  0.5,  0.5,  0.0, 1.0, // top-left        
		// left face
		-0.5,  0.5,  0.5,  0.0, 1.0, // top-right
		-0.5, -0.5, -0.5,  1.0, 0.0, // bottom-left
		-0.5,  0.5, -0.5,  1.0, 1.0, // top-left       
		-0.5, -0.5, -0.5,  1.0, 0.0, // bottom-left
		-0.5,  0.5,  0.5,  0.0, 1.0, // top-right
		-0.5, -0.5,  0.5,  0.0, 0.0, // bottom-right
		// right face
		 0.5,  0.5,  0.5,  0.0, 1.0, // top-left
		 0.5,  0.5, -0.5,  1.0, 1.0, // top-right      
		 0.5, -0.5, -0.5,  1.0, 0.0, // bottom-right          
		 0.5, -0.5, -0.5,  1.0, 0.0, // bottom-right
		 0.5, -0.5,  0.5,  0.0, 0.0, // bottom-left
		 0.5,  0.5,  0.5,  0.0, 1.0, // top-left
		// bottom face          
		-0.5, -0.5, -0.5,  0.0, 1.0, // top-right
		 0.5, -0.5,  0.5,  1.0, 0.0, // bottom-left
		 0.5, -0.5, -0.5,  1.0, 1.0, // top-left        
		 0.5, -0.5,  0.5,  1.0, 0.0, // bottom-left
		-0.5, -0.5, -0.5,  0.0, 1.0, // top-right
		-0.5, -0.5,  0.5,  0.0, 0.0, // bottom-right
		// top face
		-0.5,  0.5, -0.5,  0.0, 1.0, // top-left
		 0.5,  0.5, -0.5,  1.0, 1.0, // top-right
		 0.5,  0.5,  0.5,  1.0, 0.0, // bottom-right                 
		 0.5,  0.5,  0.5,  1.0, 0.0, // bottom-right
		-0.5,  0.5,  0.5,  0.0, 0.0, // bottom-left  
		-0.5,  0.5, -0.5,  0.0, 1.0, // top-left              
	}

	PlaneStride :: 5 * size_of(f32)
    planeVertices: []f32 = 
	{
        // positions      // texture Coords (note we set these higher than 1 (together with GL_REPEAT as texture wrapping mode). 
                          // this will cause the floor texture to repeat)
         5.0, -0.5,  5.0,  2.0, 0.0,
        -5.0, -0.5,  5.0,  0.0, 0.0,
        -5.0, -0.5, -5.0,  0.0, 2.0,

         5.0, -0.5,  5.0,  2.0, 0.0,
        -5.0, -0.5, -5.0,  0.0, 2.0,
         5.0, -0.5, -5.0,  2.0, 2.0,								
    }


	WindowStride :: 5 * size_of(f32)
     windowVerticies: []f32 = 
	 {
        0.0,  0.5,  0.0,  0.0,  0.0,
        0.0, -0.5,  0.0,  0.0,  1.0,
        1.0, -0.5,  0.0,  1.0,  1.0,

        0.0,  0.5,  0.0,  0.0,  0.0,
        1.0, -0.5,  0.0,  1.0,  1.0,
        1.0,  0.5,  0.0,  1.0,  0.0,
    }


	windowPositions: [5]v3f =
	{
		{-1.5, 0.0, -0.48},
		{1.5, 0.0, 0.51},
		{0.0, 0.0, 0.7},
		{-0.3, 0.0, -2.3},
		{0.5, 0.0, -0.6},
	}

	windowShades: [len(windowPositions)]f32 = 
	{
		0.2,
		0.4,
		0.6,
		0.8,
		1.0,
	}

	WindowMesh: []window_mesh = 
	{
		{pos = windowPositions[0], distance = 0},
		{pos = windowPositions[1], distance = 0},
		{pos = windowPositions[2], distance = 0},
		{pos = windowPositions[3], distance = 0},
		{pos = windowPositions[4], distance = 0},
	}

	assert(len(WindowMesh) == len(windowPositions))
	assert(len(windowShades) == len(WindowMesh))

	WindowMeshInterface := WindowMeshInterfaceMake(&WindowMesh)

	// GL buffers/objects

	cubeVao, cubeVbo: u32
	planeVao, planeVbo: u32
	windowVao, windowVbo: u32
	filterVao, filterVbo: u32
	skyVao, skyVbo: u32

// Cube	
//--------------------------------------------------------------------------------	
	gl.GenVertexArrays(1, &cubeVao)
	gl.GenBuffers(1, &cubeVbo)

	gl.BindVertexArray(cubeVao)
	gl.BindBuffer(gl.ARRAY_BUFFER, cubeVbo)
	gl.BufferData(gl.ARRAY_BUFFER, size_of_slice(cubeVertices), &cubeVertices[0], gl.STATIC_DRAW)

	// Vertex
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, CubeStride, 0) 

	// Texture
	gl.EnableVertexAttribArray(1)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, CubeStride, 3 * size_of(f32)) 

	gl.BindVertexArray(0);

// Plane
//--------------------------------------------------------------------------------
	gl.GenVertexArrays(1, &planeVao)
	gl.GenBuffers(1, &planeVbo)

	gl.BindVertexArray(planeVao)
	gl.BindBuffer(gl.ARRAY_BUFFER, planeVbo)

	gl.BufferData(gl.ARRAY_BUFFER, size_of_slice(planeVertices), slice_data(planeVertices), gl.STATIC_DRAW)
	// Vertex
	gl.VertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, PlaneStride, 0) 
	gl.EnableVertexAttribArray(0)

	// Texture
	gl.VertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, PlaneStride, 3 * size_of(f32)) 
	gl.EnableVertexAttribArray(1)

	gl.BindVertexArray(0)


// Window	
//--------------------------------------------------------------------------------	
	gl.GenVertexArrays(1, &windowVao)
	gl.GenBuffers(1, &windowVbo)

	gl.BindVertexArray(windowVao)
	gl.BindBuffer(gl.ARRAY_BUFFER, windowVbo)

	gl.BufferData(gl.ARRAY_BUFFER, size_of_slice(windowVerticies), slice_data(windowVerticies), gl.STATIC_DRAW)
	// Vertex
	gl.VertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, WindowStride, 0) 
	gl.EnableVertexAttribArray(0)

	// Texture
	gl.VertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, WindowStride, 3 * size_of(f32)) 
	gl.EnableVertexAttribArray(1)

	gl.BindVertexArray(0)


// Filter
//--------------------------------------------------------------------------------
	gl.GenVertexArrays(1, &filterVao)
	gl.GenBuffers(1, &filterVbo)

	gl.BindVertexArray(filterVao)
	gl.BindBuffer(gl.ARRAY_BUFFER, filterVbo)

	gl.BufferData(gl.ARRAY_BUFFER, size_of(quadVertices), &quadVertices[0], gl.STATIC_DRAW)
	// Vertex
	gl.VertexAttribPointer(0, 2, gl.FLOAT, gl.FALSE, QuadStride, 0) 
	gl.EnableVertexAttribArray(0)
	
	gl.BindVertexArray(0)

// Sky
//--------------------------------------------------------------------------------
	gl.GenVertexArrays(1, &skyVao)
	
	gl.BindVertexArray(skyVao)
	gl.BindBuffer(gl.ARRAY_BUFFER, filterVbo) // We can reuse this buffer

	gl.BufferData(gl.ARRAY_BUFFER, size_of(quadVertices), &quadVertices[0], gl.STATIC_DRAW)
	// Vertex
	gl.VertexAttribPointer(0, 2, gl.FLOAT, gl.FALSE, QuadStride, 0) 
	gl.EnableVertexAttribArray(0)

	gl.VertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, QuadStride, 2 * size_of(f32)) 
	gl.EnableVertexAttribArray(1)
	
	gl.BindVertexArray(0)



// Set textures for each shader that needs them	
//--------------------------------------------------------------------------------	
	// (Cube) Just for fun, we display one texture on the outside faces, and a different one on the inside faces
	CubeShader->Use()
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, CubeTextureOutside.id)
	CubeShader->Uniform1i("texture1", 0)
	
	gl.ActiveTexture(gl.TEXTURE1)
	gl.BindTexture(gl.TEXTURE_2D, CubeTextureInside.id)
	CubeShader->Uniform1i("texture2", 1)
	
	// (Window) The window is a separate shader, so we don't interfere with the non-transparent bits when we test for gl_Front
	WindowShader->Use()
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, WindowTexture.id)
	CubeShader->Uniform1i("texture1", 0)

	// (Sky)
	SkyShader->Use()
	gl.ActiveTexture(gl.TEXTURE0 + 2)
	gl.BindTexture(gl.TEXTURE_2D, SkyTexture.id)
	SkyShader->Uniform1i("texture1", 2)
	
	// 'knossu filter'
	FilterShader->Use()
	FilterShader->Uniform1f("alpha", 0.25)

	// Framebuffers
	OffscreenBuffer := OffscreenBufferCreate(Globals.Window.fw, Globals.Window.fh)
	ScreenQuad := ScreenQuadCreate()

	t1, t2: f64
	lastFrameTime: f32
	
	gl.ClearColor(0.1, 0.05, 0.15, 1)
	// gl.ClearColor(0.4, 0.8, 0.8, 1)
	// gl.ClearColor(0.1, 0.1, 0.1, 1)


	////////////////////
	// Main loop
	////////////////////
	for Globals.timeToQuit == false
	{
		t1 = glfw.GetTime()
		
		glfw.PollEvents()
		PollKeys(GlfwContext.Window, lastFrameTime)

// The scene (we render everything to a texture!)
//--------------------------------------------------------------------------------
		projection := la.MakePerspective(la.Radians(Camera.fov), Globals.Window.aspect, 0.1, 100)
		view := la.LookAt(Camera.pos, Camera.pos + Camera.front, Camera.up)

		gl.Viewport(0, 0, OffscreenBuffer.width, OffscreenBuffer.height)
		gl.BindFramebuffer(gl.FRAMEBUFFER, OffscreenBuffer.fb)
		gl.Enable(gl.DEPTH_TEST)
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		
		// Plane
		gl.ActiveTexture(gl.TEXTURE0)
		CubeShader->Use()
		// Move the floor for lulz
		t := cast(f32)glfw.GetTime()
		// yOffset := (((math.sin(t) + 1) / 2) * 0.5) - 0.5
		gl.BindVertexArray(planeVao)
		gl.BindTexture(gl.TEXTURE_2D, FloorTexture.id)
		translation := la.MakeTranslation(identity, v3f{0, -0.025, 0})
		// translation := la.MakeTranslation(identity, v3f{0, yOffset, 0})
		CubeShader->UniformMatrix4f("model", transmute([^]f32)&translation)
		gl.DrawArrays(gl.TRIANGLES, 0, 6)


		// Cube
		CubeShader->UniformMatrix4f("view", transmute([^]f32)&view)
		CubeShader->UniformMatrix4f("projection", transmute([^]f32)&projection)
		
		translation1 := la.MakeTranslation(identity, v3f{-1, 0, -1})
		translation2 := la.MakeTranslation(identity, v3f{2, 0, 0})
		
		gl.BindTexture(gl.TEXTURE_2D, CubeTextureOutside.id)
		gl.BindVertexArray(cubeVao)
		
		// Cube 1
		model1 := translation1
		CubeShader->UniformMatrix4f("model", transmute([^]f32)&model1)
		gl.DrawArrays(gl.TRIANGLES, 0, 36)
		
		// Cube 2
		model2 := translation2
		CubeShader->UniformMatrix4f("model", transmute([^]f32)&model2)
		gl.DrawArrays(gl.TRIANGLES, 0, 36)

		// Sky
		SkyShader->Use()
		gl.BindVertexArray(skyVao)
		gl.ActiveTexture(gl.TEXTURE0)
		gl.BindTexture(gl.TEXTURE_2D, SkyTexture.id)
		gl.DrawArrays(gl.TRIANGLES, 0, 6)


		// Windows
		// Note: Have to be rendered last, as these have transparency
		WindowShader->Use()
		WindowShader->UniformMatrix4f("view", transmute([^]f32)&view)
		WindowShader->UniformMatrix4f("projection", transmute([^]f32)&projection)

		gl.BindTexture(gl.TEXTURE_2D, WindowTexture.id)
		gl.BindVertexArray(windowVao)

		// Sort windows from furthest to closest
		// We store a copy of the window mesh in a designated struct, which also holds the distance
		// We use the distance to sort the mesh from furthest to closest
		// This avoids the previous method of allocating and deallocating a hash map every frame
		for _, i in WindowMesh
		{
			using Mesh:= &WindowMesh[i]
			pp: v4f = {pos.x, pos.y, pos.z, 0}
			Mesh.distance = la.Len(Camera.pos - pp)
		}
		
		sort.reverse_sort(WindowMeshInterface)
		
		for mesh, i in WindowMesh
		{
			model := la.MakeTranslation(identity, mesh.pos)
			WindowShader->UniformMatrix4f("model", transmute([^]f32)&model)
			WindowShader->Uniform1f("red", windowShades[i])
			gl.DrawArrays(gl.TRIANGLES, 0, 6)
		}



// The texture (this is our scene rendered to a texture)
//--------------------------------------------------------------------------------
		// The viewport was changed above to be the size of the framebuffer texture
		// We have to change to back to be the size of the user window framebuffer
		// This needs to be done in order to allow the texture to scale with resizing
		// Otherwise, we end up with an inconsistent viewport and blank areas
		gl.Viewport(0, 0, Globals.Window.wCurrent, Globals.Window.hCurrent)

		gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
		gl.Disable(gl.DEPTH_TEST)
		gl.Clear(gl.COLOR_BUFFER_BIT)

		ScreenQuad->Use()
		gl.BindVertexArray(ScreenQuad.vao)
		gl.ActiveTexture(gl.TEXTURE0)
		gl.BindTexture(gl.TEXTURE_2D, OffscreenBuffer.texId)
		gl.DrawArrays(gl.TRIANGLES, 0, 6)


		if(Globals.filterEnabled)
		{
			gl.BindVertexArray(filterVao)
			FilterShader->Use()
			gl.DrawArrays(gl.TRIANGLES, 0, 6)
		}
		ToggleLowresModeIfNecessary(&OffscreenBuffer)
		
		glfw.SwapBuffers(GlfwContext.Window)
		t2 = glfw.GetTime()
		lastFrameTime = cast(f32)(t2 - t1)
	}
	glfw.Terminate()
}

CbKeys :: proc(Window: glfw.WindowHandle, key, scancode, action, mod: i32)
{
	using glfw
	if(key == KEY_Q && action == PRESS)
	{
		Globals.timeToQuit = true
	}

	if(key == KEY_F && action == PRESS)
	{
		ToggleFullscreen(Window, GlfwContext.Monitor)
	}

	if(key == KEY_C && action == PRESS)
	{
		ToggleFilter()
	}

	if(key == KEY_V && action == PRESS)
	{
		ToggleWireframe()
	}
}

CbResize :: proc(Window: glfw.WindowHandle, x, y: i32)
{
	gl.Viewport(0, 0, x, y)

	a := max(x, y)
	b := min(x, y)
	Globals.Window.aspect = f32(a) / f32(b)
	Globals.Window.wCurrent = x
	Globals.Window.hCurrent = y
}

CbMouse :: proc(Window: glfw.WindowHandle, xpos, ypos: f64)
{
	@static
	lastX, lastY: f64

	@static
	firstMouse: b32 = true

	@static
	sensitivity: f64 = 0.1

	x := xpos
	y := ypos

	if firstMouse
	{
		lastX = x
		lastY = y
		firstMouse = false
	}

	xoffset := x - lastX
	yoffset := lastY - y
	lastX = x
	lastY = y

	xoffset *= sensitivity
	yoffset *= sensitivity

	Camera.yaw += cast(f32)xoffset
	Camera.pitch += cast(f32)yoffset
	// Camera.pitch = clamp(-89, Camera.pitch, 89)
	Camera.pitch = clamp(Camera.pitch, -89, 89)
	
	dir := la.Direction(Camera.pitch, Camera.yaw)
	Camera.front = la.Normalise(dir)
}

CbScroll :: proc(Window: glfw.WindowHandle, x, y: f64)
{
	Camera.fov -= f32(y) * 2
	Camera.fov = max(10, Camera.fov)
}

CbClose :: proc(Window: glfw.WindowHandle)
{
	Globals.timeToQuit = true
}

ToggleFullscreen :: proc(PlatformWindow: glfw.WindowHandle, PlatformMonitor: glfw.MonitorHandle)
{
	using Globals
	m := glfw.GetWindowMonitor(PlatformWindow)
	if(m == nil)
	{
		Window.ww, Window.wh = glfw.GetFramebufferSize(PlatformWindow)
		Window.x, Window.y = glfw.GetWindowPos(PlatformWindow)
		glfw.SetWindowMonitor(PlatformWindow, PlatformMonitor, 0, 0, Window.fw, Window.fh, glfw.DONT_CARE)
	}
	else
	{
		glfw.SetWindowMonitor(PlatformWindow, nil, Window.x, Window.y, Window.ww, Window.wh, glfw.DONT_CARE)
	}
}

ToggleFilter :: proc()
{
	Globals.filterEnabled = !Globals.filterEnabled
}

ToggleWireframe :: proc()
{
	@static 
	modeIndex: i32
	
	@static
	mode: [2]u32 = {gl.LINE, gl.FILL}

	gl.PolygonMode(gl.FRONT_AND_BACK, mode[modeIndex])

	modeIndex = 1 - modeIndex

	
}

PollKeys :: proc(Window: glfw.WindowHandle, dt: f32)
{
	cameraSpeed: f32 = 2.5 * dt

	KeyPress :: proc(Window: glfw.WindowHandle, key: i32) -> bool
	{
		state := glfw.GetKey(Window, key)
		return state == glfw.PRESS
	}

	using glfw

	if KeyPress(Window, KEY_W)
	{
		Camera.pos += cameraSpeed * Camera.front
	}
	if KeyPress(Window, KEY_A)
	{
		Camera.pos -= la.Normalise(la.CrossProduct(Camera.front, Camera.up)) * cameraSpeed
	}

	if KeyPress(Window, KEY_S)
	{
		Camera.pos -= cameraSpeed * Camera.front
	}

	if KeyPress(Window, KEY_D)
	{
		Camera.pos += la.Normalise(la.CrossProduct(Camera.front, Camera.up)) * cameraSpeed
	}

	if KeyPress(Window, KEY_LEFT_SHIFT)
	{
		Camera.pos.y -= cameraSpeed
	}

	if KeyPress(Window, KEY_SPACE)
	{
		Camera.pos.y += cameraSpeed
	}
}

Texture2DLoadFromFile :: proc(filepath: cstring, flip: bool, wrappingMode: i32, filterMode: i32) -> gl_texture_2d
{
	using Texture: gl_texture_2d
	stbi.set_flip_vertically_on_load(cast(i32)flip)
	data := stbi.load(filepath, &w, &h, &channels, 0)
	defer stbi.image_free(data)

	TextureType :: gl.TEXTURE_2D

	if(data == nil)
	{
		fmt.eprintf("ERROR: Could not load texture '{}' from disk", filepath)
		os.exit(1)
	}

	internalFormat: i32
	format: u32
	type: u32 = gl.UNSIGNED_BYTE

	if channels == 3
	{
		internalFormat = gl.RGB
		format = gl.RGB
	}
	else if channels == 4
	{
		internalFormat = gl.RGBA
		format = gl.RGBA
		type = gl.UNSIGNED_INT_8_8_8_8_REV
	}
	else
	{
		fmt.eprintf("Expected 3 or 4 channels to be present in the texture, but got {} channels\n", channels)
		os.exit(1)
	}
	gl.GenTextures(1, &id)
	gl.BindTexture(TextureType, id)
	gl.TexImage2D(TextureType, 0, internalFormat, w, h, 0, format, type, data)
	gl.GenerateMipmap(TextureType)

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrappingMode)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrappingMode)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filterMode)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filterMode)

	size = cast(u32)(w * h * 4)
	log("Loaded {}: {}\n", filepath, Texture)
	return Texture
}

size_of_slice :: proc(slice: []$T) -> int
{
	size := len(slice) * size_of(slice[0])
	return size
}

slice_data :: proc(slice: []$T) -> rawptr
{
	d := transmute(mem.Raw_Slice)slice
	return d.data
}

CameraInit :: proc() -> camera
{
	result: camera
	using result
	fov = 75
	pos = {0, 0, 3, 1}
	front = {0 ,0, -1, 1}
	up = {0, 1, 0, 1}
	yaw = -90

	return result
}

WindowMeshInterfaceMake :: proc(mesh: ^[]window_mesh) -> sort.Interface
{
	desired_type :: type_of(mesh)
	I: sort.Interface = 
	{
		collection = mesh,
		len = proc(it: sort.Interface) -> int
		{
			m := cast(desired_type)it.collection
			return len(m)
		},

		less = proc(it: sort.Interface, i, j: int) -> bool
		{
			m := cast(desired_type)it.collection
			return (m[i].distance < m[j].distance)
		},

		swap = proc(it: sort.Interface, i, j: int)
		{
			m := cast(desired_type)it.collection
			m[i], m[j] = m[j], m[i]
		},
	}
	return I
}

ToggleLowresModeIfNecessary :: proc(Buffer: ^offscreen_buffer)
{
	lowres :: true
	
	@static
	lastModeState: bool

	@static
	isInitialized: bool

	if !isInitialized
	{
		isInitialized = true
		lastModeState = Globals.filterEnabled

		if lastModeState == lowres
		{
			OffscreenBufferUpdateResolution(Buffer, 320, 240)
		}
	}

	if lastModeState == Globals.filterEnabled
	{
		return
	}

	lastModeState = Globals.filterEnabled
	
	if lastModeState == lowres
	{
		OffscreenBufferUpdateResolution(Buffer, 320, 240)
	}
	else
	{
		OffscreenBufferUpdateResolution(Buffer, Globals.Window.fw, Globals.Window.fh)
	}
}

ScreenQuadCreate :: proc() -> screen_quad
{
	using result: screen_quad


	gl.GenVertexArrays(1, &vao)
	gl.GenBuffers(1, &vbo)
	gl.BindVertexArray(vao)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)

	gl.BufferData(gl.ARRAY_BUFFER, size_of(quadVertices), &quadVertices[0], gl.DYNAMIC_DRAW)
	
	gl.VertexAttribPointer(0, 2, gl.FLOAT, gl.FALSE, QuadStride, 0) 
	gl.EnableVertexAttribArray(0)
	
	gl.VertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, QuadStride, 2 * size_of(quadVertices[0])) 
	gl.EnableVertexAttribArray(1)


	// Shaders for the quad
	// Apply any post-processing effects here
	v ::
	`
	#version 330 core
	layout (location = 0) in vec2 aPos;
	layout (location = 1) in vec2 aTexCoords;
	out vec2 TexCoords;

	void main()
	{
		TexCoords = aTexCoords;
		gl_Position = vec4(aPos, 0, 1);
	}
	`

	f ::
	`
	#version 330 core
	in vec2 TexCoords;
	out vec4 FragColor;

	uniform sampler2D texture1;

	void main()
	{
		float r = texture(texture1, TexCoords).r;
		float g = texture(texture1, TexCoords).g;
		float b = texture(texture1, TexCoords).b;

		// Some fun colour swaps
		// FragColor = vec4(b, g, 0, 1);
		// FragColor = vec4(r, g, 0, 1);


		// Default
		FragColor = texture(texture1, TexCoords);
	}

	`
	Shader = sl.Init()
	Shader.vars, Shader.program = sl.ShaderLoadFromBinary(v, f)
	Shader->Uniform1i("texture1", 0)
	return result
}

OffscreenBufferCreate :: proc(w, h: i32) -> offscreen_buffer
{
	using result: offscreen_buffer

	width = w
	height = h

	gl.GenFramebuffers(1, &fb)
	gl.BindFramebuffer(gl.FRAMEBUFFER, fb)
	defer gl.BindFramebuffer(gl.FRAMEBUFFER, 0)

	// The texture that we will be rendering our main scene to
	gl.ActiveTexture(gl.TEXTURE0)
	gl.GenTextures(1, &texId)
	gl.BindTexture(gl.TEXTURE_2D, texId)
	
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)

	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, w, h, 0, gl.BGRA, gl.UNSIGNED_INT_8_8_8_8_REV, nil)

	// The framebuffer
	gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texId, 0)

	// Renderbuffer
	gl.GenRenderbuffers(1, &rbo)
	gl.BindRenderbuffer(gl.RENDERBUFFER, rbo)
	gl.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH24_STENCIL8, w, h)
	gl.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT, gl.RENDERBUFFER, rbo)
	
	return result
}

OffscreenBufferUpdateResolution :: proc(Buffer: ^offscreen_buffer w, h: i32)
{
	gl.BindTexture(gl.TEXTURE_2D, Buffer.texId)
	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, w, h, 0, gl.BGRA, gl.UNSIGNED_INT_8_8_8_8_REV, nil)
	
	gl.BindRenderbuffer(gl.RENDERBUFFER, Buffer.rbo)
	gl.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH24_STENCIL8, w, h)
	
	Buffer.width = w
	Buffer.height = h
}
